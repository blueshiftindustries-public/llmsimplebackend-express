import { CopilotBackend, OpenAIAdapter } from "@copilotkit/backend";
import express from "express";
import cors from "cors";

const app = express();

// Use Express's built-in middleware to parse JSON bodies
app.use(express.json());

// copilot expects the request.json object but express uses request.body
app.use((request, response, next) => {
  request.json = () => Promise.resolve(request.body);
  next();
});

// CORS Middleware setup
app.use(cors({ origin: "http://localhost:5173" }));

app.post("/api/copilotkit", async (request, response) => {
  try {
    const copilotKit = new CopilotBackend();
    copilotKit.streamHttpServerResponse(request, response, new OpenAIAdapter({}));
  } catch (err) {
    console.error("Error during request handling:", err);
    response.status(500).send("Internal Server Error");
  }
});

const port = process.env.PORT || 4201;
app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`);
});
